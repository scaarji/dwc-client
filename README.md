# Service Client

This package contains class that is used as a client in communications via common bus.

## Usage

Add package as dependency into your package.json file.

Require package in code:

    var Client = require('dwc-client');

Create new client passing connection configuration:

    var client = new Client(config);

Configuration currently has options:

    - `timeout` - number of ms to wait before throwing timeout error
    - `amqp` - options of connection with amqp, full list of options can be found in [amqp package documentation](https://www.npmjs.com/package/amqp)

Initialize client:

    client.init(onInit);

Use client to query via the common bus:

    client.query({
        service: {
            name: 'auth',
            version: 1
        },
        op: 'getToken',
        params: {
            id: 'some-token-id'
        }
    }, onResponse);

Request HAS to contain parameters `service` (which has to contain properties `name` and `version` to route request to correct service) and `op` which is used to choose operation to be executed.
