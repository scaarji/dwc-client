'use strict';
var events = require('events'),
    util = require('util');

var sinon = require('sinon');

var exchange = function(name, options) {
    return {
        name: name,
        options: options,
        publish: sinon.spy()
    };
};

var queue = function(name, options) {
    return {
        name: name,
        options: options,
        bind: sinon.spy(),
        subscribe: sinon.spy()
    };
};

var Connection = function(options) {
    events.EventEmitter.call(this);

    this.options = options;
    this.exchange = sinon.spy(function(name, options, callback) {
        process.nextTick(function() {
            callback(exchange(name, options));
        });
    });

    this.queue = sinon.spy(function(name, options, callback) {
        process.nextTick(function() {
            callback(queue(name, options));
        });
    });

    process.nextTick(function() {
        this.emit('ready');
    }.bind(this));
};


util.inherits(Connection, events.EventEmitter);


module.exports = Connection;
