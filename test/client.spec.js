
/* global describe, it, before, after, beforeEach, afterEach */
'use strict';
var amqp = require('amqp'),
    should = require('should'),
    sinon = require('sinon'),
    async = require('async'),
    uuid = require('uuid');

var Connection = require('./mocks/connection'),
    Client = require('../lib/client');

describe('Client', function() {
    var _createConnection;
    before(function() {
        _createConnection = amqp.createConnection;
        amqp.createConnection = sinon.spy(function(options) {
            return new Connection(options);
        });
    });

    afterEach(function() {
        amqp.createConnection.reset();
    });

    after(function() {
        amqp.createConnection = _createConnection;
    });

    var client;
    beforeEach(function(done) {
        client = new Client({
            amqp: {}
        });

        client.init(done);
    });

    describe('#init', function() {
        it('should create connection and exchange', function() {
            amqp.createConnection.calledOnce.should.eql(true);
            client.should.have.property('connection', amqp.createConnection.returnValues[0]);
            client.connection.exchange.calledOnce.should.eql(true);
            client.should.have.property('_exchange');
        });
    });

    describe('#getRoute', function() {
        it('should return correct route', function() {
            var obj = {name: 'service', version: 1};
            client.getRoute(obj).should.eql(obj.name + '.' + obj.version);
        });
    });

    describe('#getQueueName', function() {
        it('should return correct queue name', function() {
            var info = {name: 'service', version: 1};
            client.getQueueName(info).should.eql('queue.' + info.name + '.' + info.version);
            info = {name: 'other-service', version: 2};
            client.getQueueName(info).should.eql('queue.' + info.name + '.' + info.version);
        });
    });

    describe('#_createQueue', function() {
        it('should create queue and bind it to given route (route given)', function(done) {
            var name = 'queue-name',
                options = {exclusive: true},
                route = '#';

            client._createQueue(name, options, route, function(err, queue) {
                should.not.exist(err);
                should.exist(queue);

                client.connection.queue.calledOnce.should.eql(true);
                client.connection.queue.firstCall.args.slice(0, 2).should.eql([name, options]);

                queue.bind.calledOnce.should.eql(true);
                queue.bind.firstCall.args.should.eql([client._exchange.name, route]);

                done();
            });
        });

        it('should create queue and bind it to queue named route (route not given)', function(done) {
            var name = 'queue-name',
                options = {exclusive: true},
                route = null;

            client._createQueue(name, options, route, function(err, queue) {
                should.not.exist(err);
                should.exist(queue);

                queue.bind.firstCall.args.should.eql([client._exchange.name, queue.name]);

                done();
            });
        });
    });

    describe('#_publish', function() {
        it('should call publish method of exchange', function() {
            var args = ['service', {msg: 'message', corrId: Date.now()}, {foo: 'bar'}];
            client._publish.apply(client, args);
            client._exchange.publish.calledOnce.should.eql(true);
            client._exchange.publish.firstCall.args.should.eql(args);
        });
    });

    describe('#query', function() {
        it('should return error on empty service info', function(done) {
            client.query({
                op: 'findUser'
            }, function(err) {
                should.exist(err);

                err.message.should.match(/service info/i);

                done();
            });
        });

        it('should return error on empty op', function(done) {
            client.query({
                service: {
                    name: 'auth',
                    version: 1
                }
            }, function(err) {
                should.exist(err);

                err.message.should.match(/operation/i);

                done();
            });
        });

        it('should return error received from service', function(done) {
            var req = {
                service: {
                    name: 'test',
                    version: 1
                },
                op: 'echo'
            };
            var error = {
                message: 'some service error message',
                name: 'severe_error'
            };
            var queue = {
                name: 'random-queue-name',
                subscribe: sinon.spy(function(callback) {
                    var correlationId = client._exchange.publish.firstCall.args[1].correlationId;

                    setTimeout(function() {
                        callback({
                            correlationId: correlationId,
                            msg: {
                                error: error
                            }
                        });
                    }, 5);
                }),
                destroy: sinon.spy()
            };

            client._createQueue = sinon.spy(function(name, opts, route, callback) {
                name.should.eql('');
                opts.should.have.property('exclusive', true);
                should.not.exist(route);

                callback(null, queue);
            });
            client.timeout = 10;
            client.query(req, function(err, message) {
                should.exist(err);
                err.message.should.eql(error.message);
                err.name.should.eql(error.name);

                should.not.exist(message);

                client._createQueue.calledOnce.should.eql(true);

                client._exchange.publish.calledOnce.should.eql(true);
                client._exchange.publish.firstCall.args[0].should.eql(client.getRoute(req.service));
                client._exchange.publish.firstCall.args[1].should.have.property('msg', req);

                queue.subscribe.calledOnce.should.eql(true);
                queue.destroy.calledOnce.should.eql(true);

                done();
            });
        });

        it('should return results received from service', function(done) {
            var req = {
                service: {
                    name: 'test',
                    version: 1
                },
                op: 'sum',
                params: {
                    one: '1',
                    two: 2
                }
            };
            var result = {
                foo: 'bar',
                some: ['info']
            };
            var queue = {
                name: 'random-queue-name',
                subscribe: sinon.spy(function(callback) {
                    var correlationId = client._exchange.publish.firstCall.args[1].correlationId;

                    setTimeout(function() {
                        callback({
                            correlationId: correlationId,
                            msg: {
                                result: result
                            }
                        });
                    }, 5);
                }),
                destroy: sinon.spy()
            };

            client.timeout = 10;
            client._createQueue = sinon.spy(function(name, opts, route, callback) {
                name.should.eql('');
                opts.should.have.property('exclusive', true);
                should.not.exist(route);

                callback(null, queue);
            });
            client.query(req, function(err, message) {
                should.not.exist(err);
                should.exist(message);

                message.should.eql(result);

                client._createQueue.calledOnce.should.eql(true);

                client._exchange.publish.calledOnce.should.eql(true);
                client._exchange.publish.firstCall.args[0].should.eql(client.getRoute(req.service));
                client._exchange.publish.firstCall.args[1].should.have.property('msg', req);

                queue.subscribe.calledOnce.should.eql(true);
                queue.destroy.calledOnce.should.eql(true);

                done();
            });
        });

        it('should return timeout error on timeout', function(done) {
            var req = {
                service: {
                    name: 'test',
                    version: 1
                },
                op: 'sum',
                params: {
                    one: '1',
                    two: 2
                }
            };
            var queue = {
                name: 'random-queue-name',
                subscribe: sinon.spy(),
                destroy: sinon.spy()
            };

            client._createQueue = sinon.spy(function(name, opts, route, callback) {
                name.should.eql('');
                opts.should.have.property('exclusive', true);
                should.not.exist(route);

                callback(null, queue);
            });
            client.timeout = 10;
            client.query(req, function(err, message) {
                should.exist(err);
                err.message.should.match(/timeout/i);
                should.not.exist(message);

                queue.subscribe.calledOnce.should.eql(true);
                queue.destroy.calledOnce.should.eql(true);

                done();
            });
        });
    });

    describe('#listen', function() {
        it('should return error on invalid service info', function(done) {
            var infos = [
                    null,
                    {version: 1},
                    {name: '123'}
                ],
                worker = function(mag, callback) {};

            async.each(infos, function(info, callback) {
                client.listen(info, worker, function(err) {
                    should.exist(err);
                    err.message.should.match(/info/i);

                    callback(null);
                });
            }, done);
        });

        it('should return error on invalid worker', function(done) {
            var info = {
                    name: 'service',
                    version: 1
                },
                workers = [
                    null,
                    function(callback) {},
                    function(msg, info, callback) {}
                ];

            async.each(workers, function(worker, callback) {
                client.listen(info, worker, function(err) {
                    should.exist(err);
                    err.message.should.match(/worker/i);

                    callback(null);
                });
            }, done);
        });

        it('should pass correct parameters to worker', function(done) {
            var info = {
                name: 'service',
                version: 1
            };
            var payload = {
                foo: 'bar',
                baz: {
                    bar: 'foo'
                }
            };
            var worker = function(msg, callback) {
                should.exist(msg);
                msg.should.eql(payload);
                should.exist(callback);
                callback.should.be.type('function');

                client._createQueue.calledOnce.should.eql(true);

                done();
            };

            var queue = {
                name: client.getQueueName(info),
                subscribe: sinon.spy(function(options, callback) {
                    callback({
                        msg: payload,
                        correlationId: uuid.v4()
                    }, {}, {
                        replyTo: uuid.v4()
                    }, {
                        acknowledge: function() {}
                    });
                }),
                destroy: sinon.spy()
            };

            client._createQueue = sinon.spy(function(name, opts, route, callback) {
                name.should.eql(client.getQueueName(info));
                route.should.eql(client.getRoute(info));

                callback(null, queue);
            });

            client.listen(info, worker, function(err) {
                should.not.exist(err);
            });
        });

        it('should correctly map error from worker to message', function(done) {
            var info = {
                name: 'service',
                version: 1
            };
            var payload = {
                foo: 'bar',
                baz: {
                    bar: 'foo'
                }
            };
            var error = {
                name: 'error-name',
                message: 'error-message'
            };
            var worker = function(msg, callback) {
                callback(error);
            };
            var correlationId = uuid.v4();
            var replyTo = uuid.v4();
            var acknowledge = sinon.spy();

            var queue = {
                name: client.getQueueName(info),
                subscribe: sinon.spy(function(options, callback) {
                    callback({
                        msg: payload,
                        correlationId: correlationId
                    }, {}, {
                        replyTo: replyTo
                    }, {
                        acknowledge: acknowledge
                    });
                }),
                destroy: sinon.spy()
            };

            client._publish = sinon.spy(function(route, result, opts) {
                route.should.eql(replyTo);
                result.should.eql({
                    correlationId: correlationId,
                    msg: {
                        error: error
                    }
                });

                process.nextTick(function() {
                    acknowledge.calledOnce.should.eql(true);
                    done();
                });
            });

            client._createQueue = sinon.spy(function(name, opts, route, callback) {
                name.should.eql(client.getQueueName(info));
                route.should.eql(client.getRoute(info));

                callback(null, queue);
            });

            client.listen(info, worker, function(err) {
                should.not.exist(err);
            });
        });

        it('should correctly map result from worker to message', function(done) {
            var info = {
                name: 'service',
                version: 1
            };
            var payload = {
                foo: 'bar',
                baz: {
                    bar: 'foo'
                }
            };
            var results = {
                some: {
                    complex: 'results'
                },
                foo: 'bar'
            };
            var worker = function(msg, callback) {
                callback(null, results);
            };
            var correlationId = uuid.v4();
            var replyTo = uuid.v4();
            var acknowledge = sinon.spy();

            var queue = {
                name: client.getQueueName(info),
                subscribe: sinon.spy(function(options, callback) {
                    callback({
                        msg: payload,
                        correlationId: correlationId
                    }, {}, {
                        replyTo: replyTo
                    }, {
                        acknowledge: acknowledge
                    });
                }),
                destroy: sinon.spy()
            };

            client._publish = sinon.spy(function(route, result, opts) {
                route.should.eql(replyTo);
                result.should.eql({
                    correlationId: correlationId,
                    msg: {
                        result: results
                    }
                });

                process.nextTick(function() {
                    acknowledge.calledOnce.should.eql(true);
                    done();
                });
            });

            client._createQueue = sinon.spy(function(name, opts, route, callback) {
                name.should.eql(client.getQueueName(info));
                route.should.eql(client.getRoute(info));

                callback(null, queue);
            });

            client.listen(info, worker, function(err) {
                should.not.exist(err);
            });
        });
    });
});
